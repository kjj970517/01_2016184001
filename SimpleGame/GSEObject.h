#pragma once

#include "GSEGlobals.h"

class GSEObject
{
public:
	GSEObject();
	~GSEObject();

	void Update(float elapsedTimeInSec, GSEUpdateParams* param);

	void SetPosition(float x, float y, float depth);
	void SetRelPosition(float x, float y, float depth);
	void SetSize(float sx, float sy);
	void SetVel(float x, float y);
	void SetAcc(float x, float y);
	void SetMass(float x);
	void SetType(GSEObjectType type);
	void SetState(GSEObjectState state);
	void SetParentID(int parentID);
	void SetLifeTime(float lifeTime);
	void SetLife(float life);
	void SetApplyPhysics(bool bPhy);
	void SetCoolTime(float coolTime);
	void ResetRemainingCoolTime();
	void SetStickToParent(bool bStick);
	void SetIsFilppedTexture(bool bFlip) { m_IsFlippedTexture = bFlip; }

	void GetPosition(float* x, float* y, float* depth);
	void GetRelPosition(float* x, float* y, float* depth);
	void GetSize(float* sx, float* sy);
	void GetVel(float* x, float* y);
	void GetAcc(float* x, float* y);
	void GetMass(float* x);
	void GetType(GSEObjectType* type);
	void GetState(GSEObjectState* state);
	int GetParentID();
	float GetLife();
	float GetLifeTime();
	bool GetApplyPhysics();
	float GetRemainingCoolTime();
	bool GetStickToParent();
	void SetTextureID(int id);
	int GetTextureID();
	bool GetIsFlippedTexture() { return m_IsFlippedTexture; }
	bool m_IsCollideCeiling = false;
	// 위로 움직이는 블록
	bool m_IsElevator = false;
	float m_MaxElevateY;
	float m_MinElevateY;
	bool m_IsElevatorUp = true;
	// 캐릭터가 엘베위에 있는지
	bool m_IsOnElevator = false;

	//몬스터가 선공형인지 : 선공형=슬라임 
	bool m_MonIsAgrresive = false;
	int m_MonDirX;
	float m_MonMaxRange;
	float m_MonMaxX; // 블록 밑으로 안떨어지게
	float m_MonMinX; // 블록 밑으로 안떨어지게
	bool m_MonIsHit = false; // 몬스터가 공격당했는지
	// 칼,몬스터 데미지 
	float m_Dmg;
	// 칼 데미지 한번만 들어가게
	bool m_IsApplyDamage = false;
private:
	float m_PositionX, m_PositionY;
	float m_RelPositionX, m_RelPositionY;
	float m_Depth;
	float m_RelDepth;
	float m_SizeX, m_SizeY;
	float m_VelX, m_VelY;
	float m_AccX, m_AccY;
	float m_Mass;
	int m_Parent; // hero id, npc id: 이 obj(칼)을 생성한 주체 id -> 자기 자신에게 데미지 안주도록
	float m_LifeTime;
	float m_Life; // HP -> HP 깎는 코드는 따로 구현 필요
	bool m_ApplyPhysics;
	float m_CoolTime; // 쿨타임 설정
	float m_RemainingCoolTime; // 마지막으로 스킬을 쓰고나서 남은 쿨타임
	bool m_StickToParent;
	int m_TextureID; // 오브젝트마다 다른 텍스쳐를 그리도록
	
	// 내가 추가
	bool m_IsFlippedTexture; // 텍스쳐가 반대방향으로 뒤집어졌었는지

	GSEObjectState m_State;
	GSEObjectType m_Type;

};

