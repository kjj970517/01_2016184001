#include "stdafx.h"
#include "GSEGame.h"
#include "math.h"

GSEGame::GSEGame()
{
	//Renderer initialize
	m_renderer = new Renderer(GSE_WINDOWSIZE_X, GSE_WINDOWSIZE_Y); // 창 크기 바꾸면 렌더러에서도 창크기 알고있어야함
	m_Sound = new Sound();


	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		m_Objects[i] = NULL;
	}
	//m_HeroTexture = m_renderer->GenPngTexture("a.png"); // 렌더러에 속해있음. -> 나중에 소멸자에서 delete 필요 // 재사용 가능!
	m_LongBrickTexture1 = m_renderer->GenPngTexture("LongBrick.png");// 재사용 가능!
	m_LongBrickTexture2 = m_renderer->GenPngTexture("LongBrick2.png");// 재사용 가능!
	m_LongBrickTexture3 = m_renderer->GenPngTexture("LongBrick3.png");// 재사용 가능!
	m_LongBrickTexture4 = m_renderer->GenPngTexture("LongBrick4.png");// 재사용 가능!


	m_ShortBrickTexture1 = m_renderer->GenPngTexture("ShortBrick.png");// 재사용 가능!
	m_ShortBrickTexture2 = m_renderer->GenPngTexture("ShortBrick4.png");// 재사용 가능! // 돌
	m_ShortBrickTexture3 = m_renderer->GenPngTexture("ShortBrick3.png");// 재사용 가능!



	// 캐릭터 텍스쳐 로드
	m_HeroSpriteTexture = m_renderer->GenPngTexture("Hero_Idle.png");// 재사용 가능!
	m_HeroJumpTexture = m_renderer->GenPngTexture("Hero_Jump.png");// 재사용 가능!
	m_HeroFallTexture = m_renderer->GenPngTexture("Hero_Fall.png");// 재사용 가능!
	m_HeroWalkSpriteTexture = m_renderer->GenPngTexture("Hero_Run.png");// 재사용 가능!
	m_HeroHitSpriteTexture = m_renderer->GenPngTexture("Hero_Hit.png");// 재사용 가능!
	m_HeroWeaponSprite = m_renderer->GenPngTexture("Weapon.png");
	// 몹 텍스쳐 로드
	m_RadishIdleSprite = m_renderer->GenPngTexture("Radish_Idle.png"); // 걍 맞으면 도망가는 무 (30x38)
	m_RadishWalkSprite = m_renderer->GenPngTexture("Radish_Run.png");
	m_RadishHitSprite = m_renderer->GenPngTexture("Radish_Hit.png");

	m_SlimeIdleSprite = m_renderer->GenPngTexture("Slime_Idle-Run.png");
	m_SlimeHitSprite = m_renderer->GenPngTexture("Slime_Hit.png");


	// 배경 텍스쳐
	m_TitleTexture = m_renderer->GenPngTexture("Title.png");
	m_EndTexture = m_renderer->GenPngTexture("ending.png");
	m_BGTexture = m_renderer->GenPngTexture("bg.png");// 재사용 가능! -> 배경
	
	m_rainTexture = m_renderer->GenPngTexture("particle_rain.png");// 재사용 가능! -> 파티클 그릴때도 텍스쳐 필요


	m_rainParticle = m_renderer->CreateParticleObject(
		3000, //파티클 개수
		-500, -500, 1500, 1500, //이 범위 안에서 만들어지도록
		10, 10, 10, 10, // 사이즈가 이범위 안에서 다양하게
		100, -100, 100,-100 // 오른쪽 아래로 떨어지게 속도
	);
	//m_trailParticle =

	//Create Monster
	// 1층-1
	m_RadishID = AddObject(5, -4.5, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_RadishID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[m_RadishID]->SetApplyPhysics(true);
	m_Objects[m_RadishID]->SetLife(100.f);
	m_Objects[m_RadishID]->SetLifeTime(100000000.f);
	m_Objects[m_RadishID]->SetTextureID(m_RadishIdleSprite); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMinX = -5.f; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMaxX = 15.f; // 여기서 오브젝트별로 텍스쳐 set


	// 1층-2
	m_RadishID = AddObject(10, -4.5, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_RadishID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[m_RadishID]->SetApplyPhysics(true);
	m_Objects[m_RadishID]->SetLife(100.f);
	m_Objects[m_RadishID]->SetLifeTime(100000000.f);
	m_Objects[m_RadishID]->SetTextureID(m_RadishIdleSprite); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMinX = -5.f; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMaxX = 15.f; // 여기서 오브젝트별로 텍스쳐 set
															 
	// 2층-1
	m_RadishID = AddObject(2.5, -2.5, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_RadishID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[m_RadishID]->SetApplyPhysics(true);
	m_Objects[m_RadishID]->SetLife(100.f);
	m_Objects[m_RadishID]->SetLifeTime(100000000.f);
	m_Objects[m_RadishID]->SetTextureID(m_RadishIdleSprite); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMinX = -2.5f; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMaxX = 7.5f; // 여기서 오브젝트별로 텍스쳐 set


	// 3층 슬라임
	m_SlimeID = AddObject(8, -1, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_SlimeID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[m_SlimeID]->SetApplyPhysics(true);
	m_Objects[m_SlimeID]->m_MonIsAgrresive = true;
	m_Objects[m_SlimeID]->SetLife(100.f);
	m_Objects[m_SlimeID]->SetLifeTime(100000000.f);
	m_Objects[m_SlimeID]->SetTextureID(m_SlimeIdleSprite); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_SlimeID]->m_MonMinX = 5.5f; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_SlimeID]->m_MonMaxX = 10.5f; // 여기서 오브젝트별로 텍스쳐 set
	

	// 4층 슬라임
	m_SlimeID = AddObject(6, 2, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_SlimeID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[m_SlimeID]->SetApplyPhysics(true);
	m_Objects[m_SlimeID]->m_MonIsAgrresive = true;
	m_Objects[m_SlimeID]->SetLife(100.f);
	m_Objects[m_SlimeID]->SetLifeTime(100000000.f);
	m_Objects[m_SlimeID]->SetTextureID(m_SlimeIdleSprite); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_SlimeID]->m_MonMinX = 1.5f; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_SlimeID]->m_MonMaxX = 5.5f; // 여기서 오브젝트별로 텍스쳐 set

	// 5층 슬라임
	m_SlimeID = AddObject(5, 5, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_SlimeID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[m_SlimeID]->SetApplyPhysics(true);
	m_Objects[m_SlimeID]->m_MonIsAgrresive = true;
	m_Objects[m_SlimeID]->SetLife(100.f);
	m_Objects[m_SlimeID]->SetLifeTime(100000000.f);
	m_Objects[m_SlimeID]->SetTextureID(m_SlimeIdleSprite); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_SlimeID]->m_MonMinX = 1.0f; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_SlimeID]->m_MonMaxX = 4.5f; // 여기서 오브젝트별로 텍스쳐 set
	// 무
	m_RadishID = AddObject(5, 5, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_RadishID]->SetType(GSEObjectType::TYPE_MOVABLE);
	m_Objects[m_RadishID]->SetApplyPhysics(true);
	m_Objects[m_RadishID]->SetLife(100.f);
	m_Objects[m_RadishID]->SetLifeTime(100000000.f);
	m_Objects[m_RadishID]->SetTextureID(m_RadishIdleSprite); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMinX = 1.0f; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[m_RadishID]->m_MonMaxX = 4.5f; // 여기서 오브젝트별로 텍스쳐 set


	//Create Hero
	m_HeroID = AddObject(-4.5, -4.5, 0, 0.5, 0.5, 0, 0, 0, 0, 25);
	m_Objects[m_HeroID]->SetType(GSEObjectType::TYPE_HERO); 
	m_Objects[m_HeroID]->SetApplyPhysics(true);
	m_Objects[m_HeroID]->SetLife(100.f);
	m_Objects[m_HeroID]->SetLifeTime(100000000.f);
	m_Objects[m_HeroID]->SetTextureID(m_HeroSpriteTexture); // 여기서 오브젝트별로 텍스쳐 set
	

	// 1층 바닥
	int floor = AddObject(0, -4.75, 0, 10, 0.5, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_LongBrickTexture3); // 여기서 오브젝트별로 텍스쳐 set
	floor = AddObject(10, -4.75, 0, 10, 0.5, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_LongBrickTexture3); // 여기서 오브젝트별로 텍스쳐 set

	// 2층
	floor = AddObject(+2.5, -2.8, 0, 10, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_LongBrickTexture1); // 여기서 오브젝트별로 텍스쳐 set

	// 3층
	floor = AddObject(8, -1, 0, 10, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_LongBrickTexture1); // 여기서 오브젝트별로 텍스쳐 set
	// 3층 좌측 위
	floor = AddObject(-4.0, 2, 0, 2, 0.4, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_ShortBrickTexture2); // 여기서 오브젝트별로 텍스쳐 set
	// 3층->4층 엘리베이터
	int elevator = AddObject(0, -1, 0, 2, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[elevator]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[elevator]->SetApplyPhysics(true);
	m_Objects[elevator]->SetLife(100000000.f);
	m_Objects[elevator]->SetLifeTime(100000000.f);
	m_Objects[elevator]->SetTextureID(m_LongBrickTexture2); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[elevator]->m_IsElevator =true; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[elevator]->m_MaxElevateY = 2.f; 
	m_Objects[elevator]->m_MinElevateY = -1.f; 
	// 4층
	floor = AddObject(6, 2, 0, 10, 0.4, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_LongBrickTexture3); // 여기서 오브젝트별로 텍스쳐 set

	// 4층->5층 엘리베이터
	elevator = AddObject(13, 2, 0, 2, 0.3, 0, 0, 0, 0, 10000);
	m_Objects[elevator]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[elevator]->SetApplyPhysics(true);
	m_Objects[elevator]->SetLife(100000000.f);
	m_Objects[elevator]->SetLifeTime(100000000.f);
	m_Objects[elevator]->SetTextureID(m_LongBrickTexture2); // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[elevator]->m_IsElevator = true; // 여기서 오브젝트별로 텍스쳐 set
	m_Objects[elevator]->m_MaxElevateY = 5.f;
	m_Objects[elevator]->m_MinElevateY = 2.f;

	// 5층
	floor = AddObject(5, 5, 0, 8, 0.4, 0, 0, 0, 0, 10000);
	m_Objects[floor]->SetType(GSEObjectType::TYPE_FIXED);
	m_Objects[floor]->SetApplyPhysics(true);
	m_Objects[floor]->SetLife(100000000.f);
	m_Objects[floor]->SetLifeTime(100000000.f);
	m_Objects[floor]->SetTextureID(m_LongBrickTexture4);

	m_BGSound = m_Sound->CreateBGSound("bgsound.mp3"); // 긴 사운드는 CreateBGSound()로
	m_Sound->PlayBGSound(m_BGSound/*인덱스*/, true/*루프여부*/, 1.f/*볼륨*/);
	m_SwordSound= m_Sound->CreateShortSound("swordSound.mp3");
}

GSEGame::~GSEGame()
{
	//Renderer delete
}


void GSEGame::Update(float elapsedTimeInSec, GSEInputs* inputs)
{
	//do garbage collecting
	DoGarbageCollect();

	GSEUpdateParams othersParam;
	GSEUpdateParams heroParam;
	memset(&othersParam, 0, sizeof(GSEUpdateParams));
	memset(&heroParam, 0, sizeof(GSEUpdateParams));

	//calc force
	float forceAmount = 400.f;

	GSEObjectState objstate;
	m_Objects[m_HeroID]->GetState(&objstate);
	if (inputs->KEY_R)
	{
		m_bIsPlaying = true;
	}


	if (m_bIsPlaying)
	{
		if (m_MonsterNum <= 0)
			m_bIsEnd = true;
		//cout << m_MonsterNum << endl;
		if (inputs->KEY_W)
		{
			if (!m_Objects[m_HeroID]->m_IsCollideCeiling)
				heroParam.forceY += 20.5 * forceAmount;
			m_Objects[m_HeroID]->SetTextureID(m_HeroJumpTexture); // 여기서 오브젝트별로 텍스쳐 set
		}

		if (inputs->KEY_A)
		{
			float x, y, z;
			m_Objects[m_HeroID]->GetPosition(&x, &y, &z);
			//if(x > -5.f) // 맵 못넘어가게
			heroParam.forceX -= forceAmount;
			/*else
				heroParam.forceX += forceAmount;*/


			m_Objects[m_HeroID]->SetIsFilppedTexture(true);
			if (objstate == GSEObjectState::STATE_GROUND)
				m_Objects[m_HeroID]->SetTextureID(m_HeroWalkSpriteTexture); // 여기서 오브젝트별로 텍스쳐 set

		}
		if (inputs->KEY_S)
		{
			heroParam.forceY -= forceAmount;
		}
		if (inputs->KEY_D)
		{
			float x, y, z;
			m_Objects[m_HeroID]->GetPosition(&x, &y, &z);
			//if (x < 15.f) // 맵 못넘어가게
			heroParam.forceX += forceAmount;
			/*else
				heroParam.forceX -= forceAmount;*/

			m_Objects[m_HeroID]->SetIsFilppedTexture(false);
			if (objstate == GSEObjectState::STATE_GROUND)
				m_Objects[m_HeroID]->SetTextureID(m_HeroWalkSpriteTexture); // 여기서 오브젝트별로 텍스쳐 set
		}
		if (objstate == GSEObjectState::STATE_GROUND && !inputs->KEY_A && !inputs->KEY_D)
		{
			m_Objects[m_HeroID]->SetTextureID(m_HeroSpriteTexture); // 여기서 오브젝트별로 텍스쳐 set
		}
		if (objstate == GSEObjectState::STATE_FALLING && !inputs->KEY_W && !m_Objects[m_HeroID]->m_IsOnElevator)
			m_Objects[m_HeroID]->SetTextureID(m_HeroFallTexture);

		if (objstate == GSEObjectState::STATE_GROUND)
			m_Objects[m_HeroID]->m_IsCollideCeiling = false;

		//sword
		float swordPosX = 0.f;
		float swordPosY = 0.f;

		if (inputs->ARROW_LEFT) 
		{
			swordPosX += -0.5f; 
		}
		if (inputs->ARROW_RIGHT)
		{
			swordPosX += 0.5f;
		}
		if (inputs->ARROW_DOWN) swordPosY += -0.5f;
		if (inputs->ARROW_UP) swordPosY += 0.5f;
		float swordDirSize = sqrtf(swordPosX * swordPosX + swordPosY * swordPosY);
		if (swordDirSize > 0.f)
		{
			// 노멀라이즈 해서 정확한 방향을 계산 -> 캐릭터 사이즈에 맞춰 1m로 계산
			float norDirX = swordPosX / swordDirSize;
			float norDirY = swordPosY / swordDirSize;

			float aX, aY, asX, asY;
			float bX, bY, bsX, bsY;
			float temp;

			m_Objects[m_HeroID]->GetPosition(&aX, &aY, &temp); // 주인공 위치 get
			m_Objects[m_HeroID]->GetSize(&asX, &asY); // 주인공 사이즈 get

			if (m_Objects[m_HeroID]->GetRemainingCoolTime() < 0.f)
			{
				int swordID = AddObject(0.f, 0.f, 0.f, 1.f/*사이즈*/, 1.f/*사이즈*/, 0.f, 0.f, 0.f, 0.f, 1.f);
				m_Objects[swordID]->SetType(GSEObjectType::TYPE_SWORD); // parent id를 hero id로
				m_Objects[swordID]->SetParentID(m_HeroID); // parent id를 hero id로
				m_Objects[swordID]->SetApplyPhysics(true);
				m_Objects[swordID]->SetRelPosition(norDirX, norDirY, 0.f); // 대각선 위,아래를 상대 좌표로 계산하도록 *상대좌표:parent 와의 상대좌표
																			// e.g.) 주인공(0,0)에 있고 칼을 (1,1)에 줬으면 주인공을 기준으로 대각선 위에 센터가 (1,1)인 위치에 칼을 생성
				m_Objects[swordID]->SetStickToParent(true); // sticktoparent가 true면 상대좌표만 사용하도록=>physics 꺼짐, false면 그냥 설정된 position 사용
				m_Objects[swordID]->SetLife(100.f);
				m_Objects[swordID]->SetTextureID(m_HeroWeaponSprite);

				m_Objects[swordID]->SetLifeTime(0.3f); // 0.3초후 자동 삭제
				m_Objects[m_HeroID]->ResetRemainingCoolTime(); // 쿨타임 리셋

				m_Objects[swordID]->m_Dmg = 100.f; // 칼 데미지

				m_Sound->PlayShortSound(m_SwordSound, false, 1.f); //addobject 되자마자 이펙트 사운드 재생
			}
		}
	}
	//Processing collision
	bool isCollide[GSE_MAX_OBJECTS];
	memset(isCollide, 0, sizeof(bool) * GSE_MAX_OBJECTS);
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		for (int j = i + 1; j < GSE_MAX_OBJECTS; j++)
		{
			if (m_Objects[i] != NULL && m_Objects[j] != NULL
				&&
				m_Objects[i]->GetApplyPhysics() && m_Objects[j]->GetApplyPhysics())
			{
				bool collide = ProcessCollision(m_Objects[i], m_Objects[j]);
				if (collide)
				{
					isCollide[i] = true;
					isCollide[j] = true;
				}
			}
		}
	}

	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			if (!isCollide[i])
			{
				m_Objects[i]->SetState(GSEObjectState::STATE_FALLING);
			}
		}
	}
	
	//Update All Objects
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			if (i == m_HeroID)
			{
				m_Objects[i]->Update(elapsedTimeInSec, &heroParam);
			}
			else
			{
				if (m_Objects[i]->GetStickToParent()) // sticktoparent = true 일 경우에는
				{
					float posX, posY, depth;
					float relPosX, relPosY, relDepth;
					int parentID = m_Objects[i]->GetParentID();
					m_Objects[parentID]->GetPosition(&posX, &posY, &depth); // parent 포지션 받아와서
					m_Objects[i]->GetRelPosition(&relPosX, &relPosY, &relDepth);
					m_Objects[i]->SetPosition(posX + relPosX, posY + relPosY, depth + relDepth); // 부모 기반으로 구한 포지션으로 실제로 옮겨준다 -> 충돌처리 가능하게
					m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
				}
				else
				{
					m_Objects[i]->Update(elapsedTimeInSec, &othersParam);
				}
			}
		}
	}
	// 월드를 넓힌 다음 카메라 포지션을 캐릭터에 고정시켜 월드를 돌아다니게
	if (m_bIsPlaying && !m_bIsEnd)
	{
		float x, y, z; // 캐릭터 위치 받아오기
		m_Objects[m_HeroID]->GetPosition(&x, &y, &z);
		float tempY = 1;
		float tempX = 1;
		float calcX = 1;
		if (y > 0)
			tempY = 100;
		if (x > 0.f)
		{
			tempX = 100;
		}
		if (x * 100 >= 1000)
		{
			calcX = 1000;
			m_renderer->SetCameraPos(calcX, y*tempY); //픽셀 단위로 *100해줘야 됨. => 카메라가 캐릭터 따라다님
		}
		else
			m_renderer->SetCameraPos(x*tempX, y*tempY); //픽셀 단위로 *100해줘야 됨. => 카메라가 캐릭터 따라다님
	}
	else if (m_bIsEnd)
	{
		m_renderer->SetCameraPos(0,0);
	}
	//cout << x * tempX << endl;
}

bool GSEGame::ProcessCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType, bType;
	a->GetType(&aType);
	b->GetType(&bType);
	
	bool isCollide = AABBCollision(a, b);

	if (isCollide)
	{
		if (a->m_IsElevator || b->m_IsElevator)
		{
			a->m_IsOnElevator = true;
			a->m_IsOnElevator = true;
		}
		else
		{
			a->m_IsOnElevator = false;
			a->m_IsOnElevator = false;
		}
		//do something -> 여기서 parent id 체크해서 데미지 안받게 ..
		if (aType == GSEObjectType::TYPE_SWORD || bType == GSEObjectType::TYPE_SWORD)
		{
			if (aType == GSEObjectType::TYPE_MOVABLE)
			{
				cout << "a여까지는 들어감" << endl;
				//cout << "b가칼 뎀:" << b->m_Dmg << endl;
				if (!b->m_IsApplyDamage)
				{
					if(!a->m_MonIsAgrresive)
						a->SetTextureID(m_RadishHitSprite);
					else
						a->SetTextureID(m_SlimeHitSprite);
					a->m_MonIsHit = true;
					a->SetLife(a->GetLife() - b->m_Dmg);
					cout << a->GetLife() << endl;
					b->m_IsApplyDamage = true;
				}
			}
			else if (bType == GSEObjectType::TYPE_MOVABLE)
			{
				//cout << "a가칼 뎀:" << a->m_Dmg << endl;
				cout << "b여까지는 들어감" << endl;
				if (!a->m_IsApplyDamage)
				{
					if (!a->m_MonIsAgrresive)
						b->SetTextureID(m_RadishHitSprite);
					else
						b->SetTextureID(m_SlimeHitSprite);
					b->m_MonIsHit = true;
					b->SetLife(b->GetLife() - a->m_Dmg);
					cout << b->GetLife() << endl;
					a->m_IsApplyDamage = true;
				}
			}
		}
		if (aType == GSEObjectType::TYPE_FIXED || bType == GSEObjectType::TYPE_FIXED)
		{
			a->SetState(GSEObjectState::STATE_GROUND);
			b->SetState(GSEObjectState::STATE_GROUND);
		}
	}
	return isCollide;
}

bool GSEGame::AABBCollision(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if (aMinX > bMaxX) // || fabs(aMinX-bMaxX)<FLT_EPSILON
	{
		return false;
	}
	if (aMaxX < bMinX)
	{
		return false;
	}
	if (aMinY > bMaxY)
	{
		return false;
	}
	if (aMaxY < bMinY)
	{
		return false;
	}
	
	AdjustPosition(a, b);
	//ApplyDamage(a, b);
	return true;
}

void GSEGame::AdjustPosition(GSEObject* a, GSEObject* b)
{
	GSEObjectType aType;
	GSEObjectType bType;

	float aMinX, aMaxX, aMinY, aMaxY;
	float bMinX, bMaxX, bMinY, bMaxY;
	float aX, aY, asX, asY;
	float bX, bY, bsX, bsY;
	float temp;

	a->GetType(&aType);
	b->GetType(&bType);

	a->GetPosition(&aX, &aY, &temp);
	a->GetSize(&asX, &asY);
	b->GetPosition(&bX, &bY, &temp);
	b->GetSize(&bsX, &bsY);

	aMinX = aX - asX / 2.f;
	aMaxX = aX + asX / 2.f;
	aMinY = aY - asY / 2.f;
	aMaxY = aY + asY / 2.f;
	bMinX = bX - bsX / 2.f;
	bMaxX = bX + bsX / 2.f;
	bMinY = bY - bsY / 2.f;
	bMaxY = bY + bsY / 2.f;

	if ((aType == GSEObjectType::TYPE_MOVABLE || aType == GSEObjectType::TYPE_HERO)
		&&
		bType == GSEObjectType::TYPE_FIXED)
	{
		if (aMaxY > bMaxY)
		{
			aY = aY + (bMaxY - aMinY);

			a->SetPosition(aX, aY, 0.f);

			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);
		}
		else // a 캐릭터 머리랑 b 블록 아래쪽
		{
			aY = aY - (aMaxY - bMinY);
			a->m_IsCollideCeiling = true;
			a->SetPosition(aX, aY-0.2f, 0.f);
			float vx, vy;
			a->GetVel(&vx, &vy);
			a->SetVel(vx, 0.f);
		}
	}
	else if (
		(bType == GSEObjectType::TYPE_MOVABLE || bType == GSEObjectType::TYPE_HERO)
		&&
		(aType == GSEObjectType::TYPE_FIXED)
		)
	{
		if (!(bMaxY > aMaxY && bMinY < aMinY))
		{
			if (bMaxY > aMaxY)
			{
				bY = bY + (aMaxY - bMinY);

				b->SetPosition(bX, bY, 0.f);
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(vx, 0.f);
			}
			else
			{
				bY = bY - (bMaxY - aMinY);

				b->SetPosition(bX, bY, 0.f);
				b->m_IsCollideCeiling = true;
				float vx, vy;
				b->GetVel(&vx, &vy);
				b->SetVel(vx, 0.f);
			}
		}
	}
}

void GSEGame::ApplyDamage(GSEObject * a, GSEObject * b)
{
	GSEObjectType aType;
	GSEObjectType bType;
	a->GetType(&aType);
	b->GetType(&bType);
	if (aType == GSEObjectType::TYPE_MOVABLE && bType == GSEObjectType::TYPE_SWORD)
	{
		a->SetLife(a->GetLife() - 100);
		//cout << "b칼적용" << endl;
	}
	else if (bType == GSEObjectType::TYPE_MOVABLE && aType == GSEObjectType::TYPE_SWORD)
	{
		b->SetLife(b->GetLife() - 100);
		//cout << "a칼적용" << endl;
	}
}

void GSEGame::DoGarbageCollect() // 가비지 콜렉터
{
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] != NULL)
		{
			float life = m_Objects[i]->GetLife(); // HP가 없거나
			float lifeTime = m_Objects[i]->GetLifeTime(); // 라이프타임이 다 깎였으면
			if (life <= 0.f || lifeTime < 0.f) 
			{
				if (m_Objects[i]->GetParentID() != m_HeroID)
					m_MonsterNum--;
				DeleteObject(i); //오브젝트 삭제
			}
		}
	}
}

int temp = 0;
float tempTime = 0.f; // 테스트용 타임-> 트레일 같은 파티클은 오브젝트마다 타임 다 따로 있어야함
float fps = 0.f;
void GSEGame::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// Renderer Test
	//m_renderer->DrawSolidRect(0, 0, 0, 100, 1, 0, 1, 1);

	//Draw Background :캐릭터,장애물보다 뒤에 그려지게
	int bgX, bgY;
	
	m_renderer->DrawGround(0, 0, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1,1, m_BGTexture);
	m_renderer->DrawGround(0, 920, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_BGTexture);
	m_renderer->DrawGround(-920, 0, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_BGTexture);
	m_renderer->DrawGround(-920, 920, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_BGTexture);
	m_renderer->DrawGround(920, 0, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_BGTexture);
	m_renderer->DrawGround(920, 920, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_BGTexture);
	m_renderer->DrawGround(1840, 0, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_BGTexture);
	m_renderer->DrawGround(1840, 920, 0, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_BGTexture);





	//Draw All Objects
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (!m_bIsEnd)
		{
			if (m_Objects[i] != NULL)
			{
				float x, y, depth;
				m_Objects[i]->GetPosition(&x, &y, &depth);
				float sx, sy;
				m_Objects[i]->GetSize(&sx, &sy);

				// 여기서 텍스쳐 id get
				int textureID = m_Objects[i]->GetTextureID();
				GSEObjectType type;
				m_Objects[i]->GetType(&type);
				int totalX, currXVal;
				if (type == GSEObjectType::TYPE_HERO)
				{
					if (textureID == m_HeroWalkSpriteTexture)
					{
						totalX = 12;
						currXVal = 13;
					}
					else if (textureID == m_HeroFallTexture)
					{
						totalX = 1;
						currXVal = 1;
					}
					else if (textureID == m_HeroJumpTexture)
					{
						totalX = 1;
						currXVal = 1;
					}
					else if (textureID == m_HeroHitSpriteTexture)
					{

					}
					else if (textureID == m_HeroSpriteTexture)
					{
						totalX = 11;
						currXVal = 12;
					}
				}
				else if (type == GSEObjectType::TYPE_MOVABLE)
				{
					if (textureID == m_RadishIdleSprite)
					{
						totalX = 6;
						currXVal = 7;
					}
					else if (textureID == m_RadishWalkSprite)
					{
						totalX = 12;
						currXVal = 13;
					}
					else if (textureID == m_RadishHitSprite)
					{
						totalX = 5;
						currXVal = 6;
					}
					else if (textureID == m_SlimeIdleSprite)
					{
						totalX = 10;
						currXVal = 11;
					}
					else if (textureID == m_SlimeHitSprite)
					{
						if (m_Objects[i]->m_MonIsAgrresive)
						{
							totalX = 5;
							currXVal = 6;
						}
					}
				}
				else if (type == GSEObjectType::TYPE_SWORD)
				{
					totalX = 5;
					currXVal = 6;
				}

				//meter to pixel
				x = x * 100.f;
				y = y * 100.f;
				sx = sx * 100.f;
				sy = sy * 100.f;
				if (m_bIsPlaying)
				{
					// 텍스쳐 없는 놈은 분기문으로 그냥 rect로 그려지게 
					if (textureID < 0)
					{
						m_renderer->DrawSolidRect(x, y, depth, sx, sy, 0.f, 1, 0, 1, 1);
					}
					else
					{
						// 스프라이트 텍스쳐는 DrawTextureRectAnim 사용
						if (type == GSEObjectType::TYPE_HERO || type == GSEObjectType::TYPE_MOVABLE || type == GSEObjectType::TYPE_SWORD)
						{
							if (m_Objects[i]->GetIsFlippedTexture())
							{
								sx *= -1;
							}
							m_renderer->DrawTextureRectAnim(
								x, y, depth,
								sx, sy, 1.f,
								1.f, 1.f, 1.f, 1.f,
								textureID,
								totalX,
								1,
								temp,
								0);

							if (fps > 0.08f)
							{
								temp++;
								fps = 0.f;
							}
							temp = temp % currXVal; // 0~12까지 왔다갔다
						}
						else // 일반 텍스쳐
						{
							m_renderer->DrawTextureRect(x, y, depth,
								sx, -sy, 1.f, // sz는 무시되기 때문에 그냥 1.f로
								1.f, 1.f, 1.f, 1.f,//rgb:텍스쳐 원본 색 그대로 넣고 싶으면 모두 1로 -> 나중에 이것도 object에서 관리해주면 편함
								textureID); // opengl에서 쓰는 좌표랑 텍스쳐 png에서 쓰는 좌표 반대기 때문에 반대로 돌려줘야함. -> 뒤집는건 알아서
						}

					}
				}
			}
		}
	}
	// 파티클은 화면 맨 앞쪽에 그려져서 맨 나중에 위치
	if (m_bIsPlaying && !m_bIsEnd)
	{
		m_renderer->DrawParticle(m_rainParticle, 0, 0, 0,
			1.f,
			1, 1, 1, 1,
			0, 0, // trail: 트레일 같은건 좁은 범위에 만든 뒤에 이 가속도 파라미터 주면 만들 수 있다. sin같은걸로 하면 왔다갔다 하는 이펙트도 가능
			m_rainTexture,
			1.f,
			tempTime
			, 0.f); // depth:0 -> 항상 앞에 그리겠다, 0.99: 항상 캐릭터 뒤에 그리겠다
	}
	tempTime += 0.016;
	fps += 0.016;

	if (!m_bIsPlaying)
	{
		m_renderer->DrawTextureRect(0, 0, 100,
			1000, -1000, 1.f, // sz는 무시되기 때문에 그냥 1.f로
			1.f, 1.f, 1.f, 1.f,//rgb:텍스쳐 원본 색 그대로 넣고 싶으면 모두 1로 -> 나중에 이것도 object에서 관리해주면 편함
			m_TitleTexture);
		//->DrawTexture(0, 0, 1, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_TitleTexture);

		
	}
	if (m_bIsEnd)
	{
		m_renderer->DrawTextureRect(0, 0, 0,
			1000, -1000, 1.f, // sz는 무시되기 때문에 그냥 1.f로
			1.f, 1.f, 1.f, 1.f,//rgb:텍스쳐 원본 색 그대로 넣고 싶으면 모두 1로 -> 나중에 이것도 object에서 관리해주면 편함
			m_EndTexture);
		//->DrawTexture(0, 0, 1, 1000, 1000/*사이즈:픽셀단위*/, 1, 1, 1, 1, 1, m_TitleTexture);
	}
	
}

int GSEGame::AddObject(float x, float y, float depth, 
	float sx, float sy,
	float velX, float velY,
	float accX, float accY,
	float mass)
{
	//find empty slot
	int index = -1;
	for (int i = 0; i < GSE_MAX_OBJECTS; i++)
	{
		if (m_Objects[i] == NULL)
		{
			index = i;
			break;
		}
	}

	if (index < 0)
	{
		std::cout << "No empty object slot.. " << std::endl;
		return -1;
	}

	m_Objects[index] = new GSEObject();
	m_Objects[index]->SetPosition(x, y, depth);
	m_Objects[index]->SetSize(sx, sy);
	m_Objects[index]->SetVel(velX, velY);
	m_Objects[index]->SetAcc(accX, accY);
	m_Objects[index]->SetMass(mass);

	return index;
}

void GSEGame::DeleteObject(int index)
{
	if (m_Objects[index] != NULL)
	{
		delete m_Objects[index];
		m_Objects[index] = NULL;
	}
	else
	{
		std::cout << "Try to delete NULL object : " << index << std::endl;
	}
}
