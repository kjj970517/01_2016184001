#pragma once
#include "Renderer.h"
#include "GSEObject.h"
#include "GSEGlobals.h"
#include "Sound.h"

class GSEGame
{
public:
	GSEGame();
	~GSEGame();

	void RenderScene();
	int AddObject(float x, float y, float depth,
		float sx, float sy,
		float velX, float velY,
		float accX, float accY,
		float mass);
	void DeleteObject(int index);
	void Update(float elapsedTimeInSec, GSEInputs* inputs);

private:
	bool AABBCollision(GSEObject* a, GSEObject* b);
	bool ProcessCollision(GSEObject* a, GSEObject* b);
	void AdjustPosition(GSEObject* a, GSEObject* b);
	void ApplyDamage(GSEObject* a, GSEObject* b);
	void DoGarbageCollect();

	Renderer* m_renderer = NULL;
	Sound* m_Sound = NULL; // 얘도 텍스쳐처럼 쓰는 느낌
	GSEObject* m_Objects[GSE_MAX_OBJECTS];
	int m_HeroID = -1;
	int m_RadishID = -1;
	int m_SlimeID = -1;

	// 텍스쳐 아이디 설정
	int m_HeroJumpTexture = -1; // hero jump texture : 이미지 1개
	int m_HeroFallTexture = -1; // hero fall texture : 이미지 1개
	int m_LongBrickTexture1 = -1; // 흙
	int m_LongBrickTexture2 = -1; // 벽돌
	int m_LongBrickTexture3 = -1; // 풀
	int m_LongBrickTexture4 = -1; // 돌


	int m_ShortBrickTexture1 = -1;
	int m_ShortBrickTexture2 = -1;
	int m_ShortBrickTexture3 = -1;

	// 캐릭터 스프라이트
	int m_HeroSpriteTexture = -1; // 핑크가이 (32x32) : Idle
	int m_HeroWalkSpriteTexture = -1; // Walk
	int m_HeroHitSpriteTexture = -1; // Hit
	int m_HeroWeaponSprite = -1;
	// 몬스터
	// Radish 텍스쳐
	int m_RadishIdleSprite = -1; // 걍 맞으면 도망가는 무 (30x38)
	int m_RadishWalkSprite = -1;
	int m_RadishHitSprite = -1;
	// 슬라임
	int m_SlimeIdleSprite = -1; // 돌진공격 슬라임 (44x30) -> 얘는 idle,run 똑같은 스프라이트
	int m_SlimeHitSprite = -1; // 돌진공격 슬라임 (44x30) -> 얘는 idle,run 똑같은 스프라이트
	

	//int m_EnemyTrunkTexture = -1; // 원거리공격 스텀프
	// 배경
	int m_TitleTexture = -1;
	int m_EndTexture = -1;

	int m_BGTexture = -1;
	// 파티클
	int m_rainTexture = -1;
	int m_RadishParticleTexture = -1; // 무 도망갈때 파티클(16x16)
	int m_SlimeParticleTexture = -1; // 슬라임 돌진할때 파티클(16x16)
	
	// 게임 시작
	bool m_bIsPlaying = false;
	bool m_bIsEnd = false;
	int m_MonsterNum = 7;

	int m_BGSound = -1;
	int m_SwordSound = -1;
	int m_rainParticle = -1;
	int m_trailParticle = -1;

};

