#include "stdafx.h"
#include "GSEObject.h"
#include "math.h"
#include <iostream>
#include <time.h>

#define MAX_VEL 3.5f // 캐릭터 최대속도
#define ELE_VEL 0.8f
#define RAD_WALK_VEL 1.f
#define RAD_RUN_VEL 2.f
#define MON_MAX_RANGE 5.f



GSEObject::GSEObject()
{
	srand(time(nullptr));
	m_PositionX = -1000000;
	m_PositionY = -1000000;
	m_RelPositionX = -1000000;
	m_RelPositionY = -1000000;
	m_Depth = -1000000;
	m_SizeX = -1000000;
	m_SizeY = -1000000;
	m_AccX = -1000000;
	m_AccY = -1000000;
	m_VelX = -1000000;
	m_VelY = -1000000;
	m_Mass = -1000000;

	m_MaxElevateY = 0;

	m_Parent = -1;
	m_LifeTime = 0.f;
	m_Life = 0.f;
	m_ApplyPhysics = false;
	m_CoolTime = 0.8f;
	m_RemainingCoolTime = m_CoolTime;
	m_StickToParent = false;
	m_TextureID = -1;
	m_IsFlippedTexture = false;
	m_IsApplyDamage = false;

	// 몬스터
	m_MonDirX = (rand() % 3); // 0이면 대기,1이면 왼쪽,2면 오른쪽
	m_MonMaxRange = 0;
	m_Dmg = 0;
	m_State = GSEObjectState::STATE_FALLING;
	m_Type = GSEObjectType::TYPE_MOVABLE;
}

GSEObject::~GSEObject()
{

}

void GSEObject::Update(float elapsedTimeInSec, GSEUpdateParams* param)
{
	m_RemainingCoolTime -= elapsedTimeInSec; //남은 쿨타임은 update에서 지난 시간만큼 감소 -> 남은 쿨타임이 0보다 작을경우 스킬 쓸수 있게
	m_LifeTime -= elapsedTimeInSec; // 라이프타임(남은 생명) update에서 지난 시간만큼 깎고있다 -> 깎아도 되고 안깎아도 되고.. 알아서 구현
	if (m_Life <= 0)
	{
		return;
	}
	if (m_Type != GSEObjectType::TYPE_MOVABLE && m_Type != GSEObjectType::TYPE_HERO && !m_IsElevator)
		return;

	if (!m_ApplyPhysics)
		return;

	if (m_StickToParent)
		return;

	float t = elapsedTimeInSec;
	float tt = elapsedTimeInSec * elapsedTimeInSec;

	float accX = 0; 
	float accY = 0;

	if (m_IsElevator)
	{
		if (m_PositionY >= m_MaxElevateY)
		{
			m_IsElevatorUp = false;
		}
		else if (m_PositionY <= m_MinElevateY)
		{
			m_IsElevatorUp = true;
		}
		if (m_IsElevatorUp)
		{
			m_VelY = ELE_VEL;
		}
		else
		{
			m_VelY = -ELE_VEL;
		}
		m_PositionY = m_PositionY + m_VelY * t + 0.5f * accY * tt;
		
	}
	else if (m_Type == GSEObjectType::TYPE_MOVABLE)
	{
		if (!m_MonIsAgrresive) // 무
		{
			if (!m_MonIsHit) // 평소 안맞은 상태
			{
				
				m_MonMaxRange += 0.03f; // 수치 조절하면 왔다갔다 거리 늘어남
				if (m_MonMaxRange >= MON_MAX_RANGE)
				{
					m_MonDirX = rand() % 3;
					m_MonMaxRange = 0;
				}
			}
			
		}
		else // 슬라임
		{
			if (!m_MonIsHit) // 평소 안맞은 상태
			{
				m_MonMaxRange += 0.03f; // 수치 조절하면 왔다갔다 거리 늘어남
				if (m_MonMaxRange >= MON_MAX_RANGE)
				{
					m_MonDirX = rand() % 3;
					m_MonMaxRange = 0;
				}
			}
		}
		if (m_MonDirX == 1)
		{
			m_VelX = -RAD_WALK_VEL;
			if (m_MonIsHit)
				m_VelX = -RAD_RUN_VEL;
		}
		else if (m_MonDirX == 2)
		{
			m_VelX = RAD_WALK_VEL;
			if (m_MonIsHit)
				m_VelX = RAD_RUN_VEL;
		}
		else
		{
			m_VelX = 0;
			if (m_MonIsHit)
				m_MonDirX = (rand() % 2) + 1;
		}
		if (m_MonDirX == 2)
			m_IsFlippedTexture = true;
		else
			m_IsFlippedTexture = false;
		if (m_PositionX <= m_MonMinX) // 맵 못넘어가게
		{
			m_MonDirX = 2;
			m_VelX = 0;
			accX = 0;
			m_PositionX = m_MonMinX +0.2f;
		}
		else if (m_PositionX >= m_MonMaxX) // 맵 못넘어가게
		{
			m_MonDirX = 1;
			m_VelX = 0;
			accX = 0;
			m_PositionX = m_MonMaxX - 0.2f;
		}
		else
		{
			m_PositionX = m_PositionX + m_VelX * t + 0.5f * accX * tt;
			m_PositionY = m_PositionY + m_VelY * t + 0.5f * accY * tt;
		}
	}
	else // 히어로
	{
		//Calc X axis
		//friction 
		if (fabs(m_VelX) > 0.f && m_State == GSEObjectState::STATE_GROUND)
		{
			//calce temporary
			accX = param->forceX / m_Mass;

			//sum with object'acc
			accX += m_AccX;

			//calc friction
			accX += m_VelX / fabs(m_VelX) * 1.0f * (-GSE_GRAVITY);

			//check wrong friction direction
			float tempVelX = m_VelX + accX * t;
			if (m_VelX * tempVelX < 0.f)
			{
				m_VelX = 0.f;
			}
			else
			{
				m_VelX = tempVelX;
				if ((m_VelX*-1) > 0 && fabs(m_VelX) > MAX_VEL)
				{
					m_VelX = -MAX_VEL;
				}
				else if ((m_VelX*-1) < 0 && fabs(m_VelX) > MAX_VEL)
				{
					m_VelX = MAX_VEL;
				}
			}

			//update velocity
			m_VelY = 0.f;
		}
		else if (m_State == GSEObjectState::STATE_GROUND || m_State == GSEObjectState::STATE_FALLING)
		{
			//calce temporary
			accX = param->forceX / m_Mass;

			m_VelX = m_VelX + accX * t;
			if ((m_VelX*-1) > 0 && fabs(m_VelX) > MAX_VEL)
			{
				m_VelX = -MAX_VEL;
			}
			else if ((m_VelX*-1) < 0 && fabs(m_VelX) > MAX_VEL)
			{
				m_VelX = MAX_VEL;
			}
		}

		//Calc Y axis
		if (m_State == GSEObjectState::STATE_GROUND)
		{
			accY += m_AccY;


			//Jump!
			if (!m_IsCollideCeiling)
				accY += param->forceY / m_Mass;

		}
		else if (m_State == GSEObjectState::STATE_FALLING)
		{
			accY -= GSE_GRAVITY;
		}

		m_VelY = m_VelY + accY * t;

		if (m_Type == GSEObjectType::TYPE_HERO)
		{
			if (m_PositionX <= -5.f) // 맵 못넘어가게
			{
				m_VelX = 0;
				accX = 0;
				m_PositionX = -4.8f;
			}
			else if (m_PositionX >= 15.f) // 맵 못넘어가게
			{
				m_VelX = 0;
				accX = 0;
				m_PositionX = 14.9f;
			}

			else {
				//update position
				m_PositionX = m_PositionX + m_VelX * t + 0.5f * accX * tt;
				m_PositionY = m_PositionY + m_VelY * t + 0.5f * accY * tt;
			}
		}
		else
		{
			m_PositionX = m_PositionX + m_VelX * t + 0.5f * accX * tt;
			m_PositionY = m_PositionY + m_VelY * t + 0.5f * accY * tt;
		}
		if (accY > 0)
			std::cout << accY << std::endl;
	}
}

void GSEObject::SetPosition(float x, float y, float depth)
{
	m_PositionX = x;
	m_PositionY = y;
	m_Depth = depth;
}

void GSEObject::SetRelPosition(float x, float y, float depth)
{
	m_RelPositionX = x;
	m_RelPositionY = y;
	m_RelDepth = depth;
}

void GSEObject::GetPosition(float* x, float* y, float* depth)
{
	*x = m_PositionX;
	*y = m_PositionY;
	*depth = m_Depth;
}

void GSEObject::GetRelPosition(float* x, float* y, float* depth)
{
	*x = m_RelPositionX;
	*y = m_RelPositionY;
	*depth = m_RelDepth;
}

void GSEObject::SetSize(float sx, float sy)
{
	m_SizeX = sx;
	m_SizeY = sy;
}

void GSEObject::GetSize(float* sx, float* sy)
{
	*sx = m_SizeX;
	*sy = m_SizeY;
}

void GSEObject::SetVel(float x, float y)
{
	m_VelX = x;
	m_VelY = y;
}

void GSEObject::SetAcc(float x, float y)
{
	m_AccX = x;
	m_AccY = y;
}

void GSEObject::SetMass(float x)
{
	m_Mass = x;
}

void GSEObject::SetType(GSEObjectType type)
{
	m_Type = type;
}

void GSEObject::SetState(GSEObjectState state)
{
	m_State = state;
}


void GSEObject::SetParentID(int parentID)
{
	m_Parent = parentID;
}

void GSEObject::SetLifeTime(float lifeTime)
{
	m_LifeTime = lifeTime;
}

void GSEObject::SetLife(float life)
{
	m_Life = life;
}

void GSEObject::SetApplyPhysics(bool bPhy)
{
	m_ApplyPhysics = bPhy;
}

void GSEObject::SetCoolTime(float coolTime)
{
	m_CoolTime = coolTime;
}

void GSEObject::ResetRemainingCoolTime()
{
	m_RemainingCoolTime = m_CoolTime;
}

void GSEObject::SetStickToParent(bool bStick)
{
	if (m_Parent < 0)
	{
		std::cout << "This object has no parent" << std::endl;
		return;
	}

	if (m_StickToParent && !bStick)
	{
		m_PositionX = m_PositionX + m_RelPositionX;
		m_PositionY = m_PositionY + m_RelPositionY;
	}

	m_StickToParent = bStick;
}

void GSEObject::GetVel(float* x, float* y)
{
	*x = m_VelX;
	*y = m_VelY;
}

void GSEObject::GetAcc(float* x, float* y)
{
	*x = m_AccX;
	*y = m_AccY;
}

void GSEObject::GetMass(float* x)
{
	*x = m_Mass;
}

void GSEObject::GetType(GSEObjectType* type)
{
	*type = m_Type;
}

void GSEObject::GetState(GSEObjectState* state)
{
	*state = m_State;
}


int GSEObject::GetParentID()
{
	return m_Parent;
}

float GSEObject::GetLife()
{
	return m_Life;
}

float GSEObject::GetLifeTime()
{
	return m_LifeTime;
}

bool GSEObject::GetApplyPhysics()
{
	return m_ApplyPhysics;
}

float GSEObject::GetRemainingCoolTime()
{
	return m_RemainingCoolTime;
}

bool GSEObject::GetStickToParent()
{
	return m_StickToParent;
}

void GSEObject::SetTextureID(int id)
{
	m_TextureID = id;
}

int GSEObject::GetTextureID()
{
	return m_TextureID;
}
